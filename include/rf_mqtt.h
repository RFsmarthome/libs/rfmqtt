#ifndef _RF_MQTT_H
#define _RF_MQTT_H

#include <Arduino.h>
#include <mqtt_client.h>
#include <functional>

#include "freertos/semphr.h"



using connectCallback = std::function<void(void)>;
using disconnectCallback = std::function<void(void)>;
using receivedCallback = std::function<void(const String &topic, const String &payload)>;



class Mqtt
{
public:
    Mqtt();

    void start();
    void stop();

    void connect(const String &uri, const String *statusTopic = nullptr);
    void connect(const IPAddress &ip, int port, const String *statusTopic = nullptr);
    void disconnect();

    bool isConnected() const;

    void setConnectCallback(connectCallback cb);
    void setDisconnectCallback(disconnectCallback cb);
    void setReceivedCallback(receivedCallback cb);

    bool send(const String &topic, const String &payload, int qos=1, int retain=0) const;
    bool subscribe(const String &topic, int qos=1) const;

private:
    static esp_err_t eventHandler(esp_mqtt_event_t *event);

    static void mqttWatchdog(void *pvParameters);

    String uri;
    bool isConnect = false;

    static connectCallback m_connectCallback;
    static disconnectCallback m_disconnectCallback;
    static receivedCallback m_receivedCallback;

    static SemaphoreHandle_t s_lock;
    static String *m_lwtTopic;

    esp_mqtt_client_handle_t client;
    static const char *TAG;
};

#endif
