#include <Arduino.h>

#include <uuid/log.h>

#include <mqtt_client.h>
#include <esp_task.h>

#include "rf_mqtt.h"

connectCallback Mqtt::m_connectCallback = nullptr;
disconnectCallback Mqtt::m_disconnectCallback = nullptr;
receivedCallback Mqtt::m_receivedCallback = nullptr;

String *Mqtt::m_lwtTopic = nullptr;

SemaphoreHandle_t Mqtt::s_lock = nullptr;

static uuid::log::Logger mqttLogger(F("mqtt"));

Mqtt::Mqtt()
{
    client = NULL;
    if(!s_lock) {
        s_lock = xSemaphoreCreateMutex();
        xSemaphoreGive(s_lock);
    }
}

void Mqtt::start()
{
    xTaskCreate(Mqtt::mqttWatchdog, "mqtt_wd", configMINIMAL_STACK_SIZE + 2048, this, 0, nullptr);
}

void Mqtt::stop()
{
}

bool Mqtt::isConnected() const
{
    if(client) {
        return isConnect;
    }
    return false;
}

void Mqtt::connect(const IPAddress &ip, int port, const String *statusTopic)
{
    mqttLogger.trace("MQTT: Connect called");
    String url = "mqtt://"+ ip.toString() +":"+ String(port);
    connect(url, statusTopic);
}

void Mqtt::connect(const String &uri, const String *statusTopic)
{
    if(!client) {
        mqttLogger.trace("MQTT: No running client");

        mqttLogger.debug("MQTT: URI: %s\n", uri.c_str());
        this->uri = uri;

        esp_mqtt_client_config_t cfg;
        memset(&cfg, 0, sizeof(esp_mqtt_client_config_t));
        cfg.uri = this->uri.c_str();
        cfg.lwt_qos = 1;
        cfg.event_handle = eventHandler;
        cfg.user_context = this;

        String lwtMessage = "OFFLINE";
        if(statusTopic != nullptr) {
            mqttLogger.info("MQTT enable LWT");
            m_lwtTopic = new String(*statusTopic);
            cfg.lwt_topic = (*m_lwtTopic).c_str();
            cfg.lwt_qos = 1;
            cfg.lwt_retain = 1;
            cfg.keepalive = 120;
            cfg.lwt_msg = lwtMessage.c_str();
            cfg.lwt_msg_len = lwtMessage.length();
        }

        mqttLogger.trace("MQTT: Init");
        client = esp_mqtt_client_init(&cfg);
        mqttLogger.trace("MQTT: Start");
        esp_mqtt_client_start(client);
        mqttLogger.trace("MQTT: Running");
    }
}

void Mqtt::disconnect()
{
    if(client) {
        if(m_lwtTopic) {
            delete m_lwtTopic;
            m_lwtTopic = nullptr;
        }
        esp_mqtt_client_stop(client);
        client = NULL;
    }
}

void Mqtt::setConnectCallback(connectCallback cb)
{
    m_connectCallback = cb;
}

void Mqtt::setDisconnectCallback(disconnectCallback cb)
{
    m_disconnectCallback = cb;
}

void Mqtt::setReceivedCallback(receivedCallback cb)
{
    m_receivedCallback = cb;
}

bool Mqtt::send(const String &topic, const String &payload, int qos, int retain) const
{
    if(xSemaphoreTake(s_lock, portMAX_DELAY) == pdTRUE){
        if(isConnected()) esp_mqtt_client_publish(client, topic.c_str(), payload.c_str(), payload.length(), qos, retain);
        xSemaphoreGive(s_lock);
        return true;
    }
    return false;
}

bool Mqtt::subscribe(const String &topic, int qos) const
{
    mqttLogger.debug("MQTT: Subscribe to '%s'", topic.c_str());
    if(xSemaphoreTake(s_lock, portMAX_DELAY) == pdTRUE){
        if(isConnected()) esp_mqtt_client_subscribe(client, topic.c_str(), qos);
        xSemaphoreGive(s_lock);
        return true;
    }
    return false;
}

esp_err_t Mqtt::eventHandler(esp_mqtt_event_t *event)
{
    switch(event->event_id) {
    case MQTT_EVENT_CONNECTED:
        {
            mqttLogger.debug("MQTT: Connected");
            ((Mqtt*)event->user_context)->isConnect = true;
            if(m_connectCallback) m_connectCallback();
            ((Mqtt*)event->user_context)->send((*m_lwtTopic), "ONLINE", 1, 1);
        }
        break;
    case MQTT_EVENT_DISCONNECTED:
        {
            mqttLogger.debug("MQTT: Disconnected");
            ((Mqtt*)event->user_context)->isConnect = false;
            if(m_disconnectCallback) m_disconnectCallback();
        }
        break;
    case MQTT_EVENT_DATA:
        {
            mqttLogger.trace("MQTT: Data received");

            char *buffer = new char[1024];
            memcpy(buffer, event->topic, event->topic_len);
            buffer[event->topic_len] = 0;
            String *topic = new String(buffer);

            memcpy(buffer, event->data, event->data_len);
            buffer[event->data_len] = 0;
            String *payload = new String(buffer);
            delete[] buffer;

            if(m_receivedCallback) m_receivedCallback(*topic, *payload);

            delete topic;
            delete payload;
        }
        break;
    case MQTT_EVENT_PUBLISHED:
        {
            mqttLogger.trace("MQTT: PUBLISHED");
        }
        break;
    default:
        mqttLogger.trace("MQTT: Other event %d", event->event_id);
        break;
    }
    return ESP_OK;
}

void Mqtt::mqttWatchdog(void *pvParameters)
{
    mqttLogger.debug("MQTT watchdog task running");
    bool lastOkay = true;
    Mqtt *mqtt = (Mqtt*)pvParameters;
    for(;;) {
        vTaskDelay(pdMS_TO_TICKS(60000));
        mqttLogger.trace("MQTT watchdog check");
        if(!lastOkay && !mqtt->isConnect) {
            mqttLogger.crit("MQTT watchdog need reset!\n");
            esp_restart();
        }

        lastOkay = mqtt->isConnect;
    }
}
